import * as React from 'react'
import { HashRouter as Router, Link, withRouter, RouteComponentProps } from 'react-router-dom';
import {TopMenu, TopMenuItem} from '@simplus/siui'
import {Breadcrumb} from 'antd'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../robins'
import {ErrorBoundary} from '../../utils/ErrorBoundary'
const {ClientsRobin, UsersRobin} = robins;
const breadcrumbNameMap = {
	'analysis': 'Analysis',
	'cross-client-analysis': 'Cross Client Analysis',
	'client-prioritisation': 'Client Prioritisation',
	'summary': 'ICAS Summary',
	'programmeinfo': 'Programme Info',
	'dashboard': 'Dashboards',
	'engagement': 'Engagement',
	'health': 'Health',
	'effectiveness': 'Effectiveness',
	'quality': 'Quality',
	'roi': 'ROI',
	'cost-to-serve': 'Cost to Serve',
	'prioritisation': 'Prioritisation',
	'stories': 'Stories',
	'notes': 'Notes',
	'settings': 'Settings',
	'user': 'User',
	'profile': 'Profile',
	'usermanagement': 'User Management',
	'roles': 'Roles',
	'users': 'Users',
};

const BreadcrumbsBar = withRouter((props: RouteComponentProps<{}>) => {
	const { location } = props;
	const clientsList = ClientsRobin.getCollection() || [];
	const usersList = UsersRobin.getCollection() || [];
	const pathSnippets = location.pathname.split('/').filter(i => i);
	const extraBreadcrumbItems = pathSnippets.map((path, index) => {
		const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
		const client = clientsList.find(item => item._key === path)
		let user = usersList.find(item => item._id === path)
		if (index > 0 && pathSnippets[index - 1] === 'set-password') {
			user = { name : 'From token'} as any
		}
		let breadcrumb = path
		if (breadcrumbNameMap[path])
			breadcrumb = breadcrumbNameMap[path]
		else if (client)
			breadcrumb = client.Name
		else if (user)
			breadcrumb = user.name
		return (
			<Breadcrumb.Item key={url}>
				<Link to={url}>
					{breadcrumb}
				</Link>
			</Breadcrumb.Item>
		);
	});
	const breadcrumbItems = [(
	<Breadcrumb.Item key='home'>
		<Link to='/'>Home</Link>
	</Breadcrumb.Item>
	)].concat(extraBreadcrumbItems);
	return (
		<ErrorBoundary>
			<TopMenu style={{
			backgroundColor : 'rgb(60, 143, 200)',
			height: '70px'
			}}>
				<TopMenuItem>
					<Breadcrumb separator='>'>
						{breadcrumbItems}
					</Breadcrumb>
				</TopMenuItem>
			</TopMenu>
		</ErrorBoundary>
	);
});

@connectRobin([ClientsRobin, UsersRobin])
export class BreadcrumbRouter extends React.Component {
	componentWillMount(): void {
		ClientsRobin.find({})
		UsersRobin.find({})
	}
	render(): JSX.Element {
		return <Router>
			<BreadcrumbsBar />
	</Router>
	}
};

export default BreadcrumbRouter;