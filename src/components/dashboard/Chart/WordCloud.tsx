import * as React from 'react'
// import randomColor from 'randomcolor'
import ReactWordCloud from 'react-wordcloud';
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
export interface WordCloudProps {
	words: Array<{
		word: string,
		count: number
	}>
}

export class WordCloud extends React.Component<WordCloudProps> {
	componentDidMount(): void {
		window.addEventListener('resize', () => {
			this.setState({
				resize: Math.random()
			})
		})
	}
	componentWillUnmount(): void {
		window.removeEventListener('resize',() => {})
	}
	render(): JSX.Element {
		if (!this.props.words.length)
			return <div>No data</div>
		return <div style={{ width :  '100%', height: 400}}>
			<ErrorBoundary>
				<ReactWordCloud
					words={this.props.words}
					wordCountKey={'count'}
					wordKey={'word'}
				/>
			</ErrorBoundary>
		</div>
	}
}