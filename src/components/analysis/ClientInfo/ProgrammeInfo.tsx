import * as React from 'react'
import {Divider, notification, Modal, Button} from 'antd'
import {TitledCard, Card, ProfilePicture, Loader} from '@simplus/siui'
import {ListLayout, ListItem} from '@simplus/macaw-business'
import {connectRobin} from '@simplus/robin-react'
import {RouteComponentProps} from 'react-router-dom'
import {robins} from '../../../robins'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import { hasPermission } from '../../../utils';

const productsJSON = require('../../../utils/products.json')

const {ClientsRobin, clientSettings, UsersRobin, ProductsRobin, PermissionsRobin} = robins;

@connectRobin([ClientsRobin, clientSettings, UsersRobin, ProductsRobin, PermissionsRobin])
export class ProgrammeInfo extends React.Component {
	state = { visible: false, product: {name: '', description: ''}, logo: ''}
	componentWillMount(): void {
		const clientID = (this.props as RouteComponentProps<{id: string}>).match.params.id
		ClientsRobin.when(ClientsRobin.findOne(clientID)).catch(err => {
			notification.error({type: 'error', message: 'Could not load client data !', description: 'There was an error during the settings api execution.'})
		})
		clientSettings.when(clientSettings.findOne(clientID)).catch(err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		UsersRobin.when(UsersRobin.findOne(clientID)).catch(err => {
			notification.error({type: 'error', message: 'Could not load users data !', description: 'There was an error during the user api execution.'})
		})
		ProductsRobin.when(ProductsRobin.find({})).catch(err => {
			notification.error({type: 'error', message: 'Could not load products data !', description: 'There was an error during the products api execution.'})
		})
	}
	showModal = () => {
		this.setState({
			visible: true,
		});
	}
	handleOk = () => {
		this.setState({
			visible: false,
		});
	}
	handleCancel = () => {
		this.setState({
			visible: false,
		});
	}
	// Below code will be used when scope is fixed
	// componentWillReceiveProps(): void {
		// const settings = props.clientSettings.model
		// const users = props.UsersRobin.model
		// if (settings && !users) {
		// 	UsersRobin.scope('icasCustodian').findOne(settings[0].icasCustodian)
		// 	UsersRobin.scope('clientCustodian').findOne(settings[0].icasCustodian)
		// }
	// }
	render(): JSX.Element {
		const client_data = ClientsRobin.getModel() || {
			Name: '',
			Industry: '',
			SegmentSize: '',
			EmployeeCount: '',
			Tier: '',
		}
		const settings = clientSettings.getModel() || [{
			img: '',
			custodian: '',
			products: []
		}]
		const allProducts = (ProductsRobin.getCollection() || [
			{product: ''},
		]).filter(product => (!!product&& (product.product!=='Absence ')))
		
		const products = (settings[0] && settings[0].products) ? settings[0].products.filter( product => !!product) : []
		const extraProducts = allProducts.filter(item => !(products || []).find(product => product === item.product))
		const allUsers = UsersRobin.getCollection() || []
		const ICASCustodian =  settings[0] ? settings[0].icasCustodian || '' : ''
		const clientCustodian =  settings[0] ? settings[0].clientCustodian || '' : ''
		// Below code will be used when scope is fixed
		// const ICASCustodian_dataArray = UsersRobin.getModel('icasCustodian') || []
		// const clientCustodian_dataArray = UsersRobin.getModel('clientCustodian') || []
		const ICASCustodian_data = allUsers.find(user => user._id === ICASCustodian) || {
			picture: '/src/assets/img/sample-profile.png',
			name: 'Unassigned',
			email: 'Unassigned'
		}
		const clientCustodian_data = allUsers.find(user => user._id === clientCustodian) || {
			name: 'Unassigned',
			email: 'Unassigned',
			picture: '/src/assets/img/sample-profile.png'
		}
		const img = settings[0] ? settings[0].img : ''
		return(
			<ErrorBoundary>
				<div className='programme-info'>
					<h1 className='analysis-tab-title'>Programme info</h1>
					<Divider/>
					<div style={{position: 'relative'}}>
						{(ClientsRobin.isLoading(ClientsRobin.ACTIONS.FIND_ONE) || ClientsRobin.isError(ClientsRobin.ACTIONS.FIND_ONE)) ? <Loader error={ClientsRobin.isError(ClientsRobin.ACTIONS.FIND_ONE)}/> : null}
						<div style={{display: 'flex'}}>
							<div>
								<TitledCard margin
									titleStyle={{ backgroundImage: `url(/src/assets/img/sector/${(client_data.Industry || 'null').split(' ').join('')}.jpg)`, backgroundColor : '#0090cd', backgroundSize: '100% 100%' }}
									rounded
									picture={img}
									className = 'client-card'>
									<h3>
										<div className='client-card-title'>{client_data.Name}</div>
										<div className='client-card-industry'>{client_data.Industry}</div>
									</h3>
									{hasPermission('/view/programme-info/icas', PermissionsRobin.getResult('own-permissions')) ? (
									<div className='client-card-content'>
										<div className='client-card-item-name'>Size segment: <span className='client-card-items'>{client_data.SegmentSize}</span></div>
										<div className='client-card-item-name'># employees: <span className='client-card-items'>{client_data.EmployeeCount}</span></div>
										<div className='client-card-item-name'>Tier: <span className='client-card-items'>{client_data.Tier}</span> </div>
									</div> )
									: null }
									<div className='custodian-row'>
										<div className='custodian-section'>
											<div className='custodian-title'>ICAS<br/>CUSTODIAN</div>
											<ProfilePicture rounded url={ICASCustodian_data.picture} size={50} />
											<div className='custodian-name'>{ICASCustodian_data.name}</div>
											<div className='custodian-email'>{ICASCustodian_data.email}</div>
										</div>
										<div className='custodian-section'>
											<div className='custodian-title'>CLIENT<br/>CUSTODIAN</div>
											<ProfilePicture rounded url={clientCustodian_data.picture} size={50} />
											<div className='custodian-name'>{clientCustodian_data.name}</div>
											<div className='custodian-email'>{clientCustodian_data.email}</div>
										</div>
									</div>
								</TitledCard>
							</div>
							<ListLayout spacebetween={10} expandable direction='vertical'>
								<ListItem style={{padding: '1rem'}}>
									<div className='product-title'>Available Products</div>
									<div className='product-section'>
									{(products || []).sort().map ( (product, index) =>
										<div key={index}
										style={{cursor: 'pointer'}}
										onClick={() => this.setState({
											product: productsJSON.find(item => item.key === product),
											logo: `/src/assets/img/Products/${(product || '').split(' ').join('').split('|').join('')}.png`,
											visible: true
										})}>
											<Card key={index} className='product-cards'>
												<div className='product-icon'>
													<img src={`/src/assets/img/Products/${(product || '').split(' ').join('').split('|').join('')}.png`}/>													
												</div>
												<div className='product-name'>{product || ''}</div>
											</Card>
										</div>
									)}
									</div>
								</ListItem>
								<ListItem style={{padding: '1rem'}}>
									<div className='product-title'>Other Products</div>
									<div className='product-section'>
									{extraProducts.map ( (item, index) =>
										<div key={index}
										style={{cursor: 'pointer'}}
										onClick={() => this.setState({
											product: productsJSON.find(product => product.key === item.product),
											logo: `/src/assets/img/OtherProducts/${(item.product || '').split(' ').join('').split('|').join('')}.png`,
											visible: true
										})}>
											<Card className='product-cards'>
												<div className='product-icon'>
													<img src={`/src/assets/img/OtherProducts/${(item.product || '').split(' ').join('').split('|').join('')}.png`}/>
												</div>
												<div className='product-name'>{item.product}</div>
											</Card>
										</div>
									)}
									</div>
								</ListItem>
							</ListLayout>
						</div>
						<Modal
							width={800}
							title={<div style={{display: 'flex', justifyContent: 'space-between'}}>
								<h3 style={{color: '#224b81', fontSize: '1.75rem', fontWeight: 400}}>{(this.state.product || {name: 'Not defined'}).name}</h3>
								<img height={25} src={this.state.logo}/>
							</div>}
							visible={this.state.visible}
							onOk={this.handleOk}
							onCancel={this.handleCancel}
							closable={false}
							footer={[<Button key='Got it' type='primary' onClick={this.handleOk}>Got it</Button>]}
							>
							<div className='programme-modal'dangerouslySetInnerHTML={{ __html: (this.state.product || {description: 'Not defined'}).description }} />
						</Modal>
					</div>
				</div>
			</ErrorBoundary>
		)
	}

}
export default ProgrammeInfo;