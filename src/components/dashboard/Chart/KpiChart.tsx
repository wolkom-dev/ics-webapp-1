import * as React from 'react'
import * as _ from 'lodash'
import {Loader} from '@simplus/siui'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
const DEFAULT_COLORS = ['#333'];
const DEFAULT_BACKGROUND = ['#FFF'];

export interface KpiChartProps{
	config?:{
		background?: string
		color?: string
		unit?: string
	}
	kpis: Array<{
		title?: string
		data: any
		footer?: any
		loading?: boolean
		error?: boolean
	}>
}

export class KpiChart extends React.Component<KpiChartProps>{
	render(): JSX.Element {
		const BACKGROUND = _.get(this.props,'config.background', DEFAULT_BACKGROUND)
		const COLOR = _.get(this.props, 'config.color', DEFAULT_COLORS)
		const unit = _.get(this.props, 'config.unit', '')
		return <ErrorBoundary>
			<div className='kpi-charts'>
				{
					this.props.kpis.map((kpi, index) => {
						const border = index < this.props.kpis.length - 1 ? '1px solid #999' : '';
						return <div
							key={index}
							className='kpi'
							style={{
								backgroundColor: BACKGROUND,
								color: COLOR
								}
							}>
								<div className='kpi-title'>{kpi.title}</div>
								<div className='kpi-data' style={{borderRight: border, position: 'relative'}}>
									{(kpi.loading || kpi.error) ? <div style={{color: 'transparent'}}><Loader error={kpi.error}/>-</div> : kpi.data}
									{unit}
								</div>
								<div className='kpi-footer' style={{position: 'relative'}}>{kpi.loading ? null : kpi.footer}</div>
						</div>
					}
				)}
			</div>
		</ErrorBoundary>
	}
}