import * as React from 'react'
import { Modal, Form, Input } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
import * as _ from 'lodash'
const FormItem = Form.Item;

interface FormProps extends FormComponentProps {
	visible: boolean,
	onCancel(): void,
	onCreate(): void,
	wrappedComponentRef?(formref: any): void
}

export const NewStory = Form.create({
	mapPropsToFields : props => {
		return _.mapValues(props, (v) => {
			return Form.createFormField({value : v})
		})
	}
})(
	class extends React.Component<FormProps> {
		render(): JSX.Element {
		const { visible, onCancel, onCreate, form } = this.props;
		const { getFieldDecorator, getFieldValue } = form;
		const key = getFieldValue('_key')
		return (
			<ErrorBoundary>
				<Modal
					visible={visible}
					title={key ? 'Edit story' : 'Create a new story'}
					okText={key ? 'Update' : 'Create'}
					onCancel={onCancel}
					onOk={onCreate}
				>
					<Form layout='vertical'>
					<Form.Item
						style={{display: 'none'}}
					>
						{getFieldDecorator('_key', {
						})
						(
							<span hidden />
						)}
					</Form.Item>
					<FormItem label='Title'>
						{getFieldDecorator('title', {
						rules: [{ required: true, message: 'Please input the title of Story!' }],
						})(
						<Input placeholder='Story title'/>
						)}
					</FormItem>
					<FormItem label='Story'  className='form_last-form-item'>
						{getFieldDecorator('text')(<Input.TextArea placeholder='Write story content' autosize={{ minRows: 6, maxRows: 12 }} />)}
					</FormItem>
					</Form>
				</Modal>
			</ErrorBoundary>
			);
		}
	}
);