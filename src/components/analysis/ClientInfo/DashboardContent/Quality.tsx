import * as React from 'react'
import {
	DashboardContainer,
	Dashboard,
	DashboardFilter,
	DashboardSection,
	DashboardTab,
	KpiChart,
	WordCloud
} from 'src/components/dashboard'
import * as moment from 'moment'
import * as queryString from 'query-string'
import * as _ from 'lodash'
import {Tabs, TabPane, Loader} from '@simplus/siui'
import {connectRobin} from '@simplus/robin-react'
import { RouteComponentProps } from 'react-router-dom'
import {notification} from 'antd'
import {robins} from 'src/robins'
import { DateRangeFilter, numberFormater, relative } from './utils';
import {hasPermission} from '../../../../utils'
import {ErrorBoundary} from '../../../../utils/ErrorBoundary'
const {clientSettings, AnalyticsRobin, PermissionsRobin} = robins

@connectRobin([clientSettings, AnalyticsRobin, PermissionsRobin])
export class Quality extends React.Component<RouteComponentProps<{id: string}>> {
	state = {filter: moment('2018-01-01'), compareBenchmark: true}
	unlisten?: () => void = undefined
	componentWillMount(): void {
		clientSettings.when(clientSettings.findOne(this.props.match.params.id)).then(() => {
			const settings = clientSettings.getModel();
			if (settings[0]) {
				const reportingCycle = moment(settings[0].reportingCycle)
				if (!reportingCycle.isSame(this.state.filter, 'd')) {
					this.setState({filter: reportingCycle})
				}
			}
			this.applyFilters()
		}).catch( err => {
			notification.error({type: 'error', message: 'Could not load settings data !', description: 'There was an error during the settings api execution.'})
		})
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		const benchmark = queryString.parse(location.hash.split('?')[1]).benchmark || 'Previous range'
		const dateRange = queryString.parse(location.hash.split('?')[1]).dateRange || 'month'

		const startDate = queryString.parse(location.hash.split('?')[1]).startDate || moment().startOf('month').subtract(1, dateRange).toISOString();
		const endDate = queryString.parse(location.hash.split('?')[1]).endDate || moment().startOf('month').toISOString();

		const options: any = {
			startdate : startDate,
			enddate : endDate,
			client : this.props.match.params.id
		}
		if (benchmark === 'Previous range') {
			options.benchmarkstartdate = moment(startDate).subtract(1, dateRange).toISOString()
			options.benchmarkenddate = moment(endDate).subtract(1, dateRange).toISOString()
		} else {
			options.benchmark = benchmark.split(',')
		}
		AnalyticsRobin.when(AnalyticsRobin.post('quality_keymetrics', '/quality/sentiments', options)).catch( err => {
			notification.error({type: 'error', message: 'Could not load quality keymetrics data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('quality_wordcloud_compliments', '/quality/wordcloud', {wordcategory : 'compliments',...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load quality wordcloud data !', description: 'There was an error during the analytics api execution.'})
		})
		AnalyticsRobin.when(AnalyticsRobin.post('quality_wordcloud_complaints', '/quality/wordcloud', {wordcategory : 'complaints',...options})).catch( err => {
			notification.error({type: 'error', message: 'Could not load quality wordcloud data !', description: 'There was an error during the analytics api execution.'})
		})
	}
	componentWillUnmount(): void {
		if (this.unlisten)
		this.unlisten();
	}
	pageRedirect(update: any, refresh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refresh)
			this.applyFilters()
	}
	urlState(): any {
		return queryString.parse(location.hash.split('?')[1])
	}
	render(): JSX.Element {
		const clientID = this.props.match.params.id
		const noPermission = <h1 className='no-permission'>You do not have permission to view this page</h1>
		// Query Strings
		const { dateRange, site, benchmark, comparison, tab} = _.defaults(this.urlState(), {
			dateRange : 'month',
			site: 'All',
			benchmark: 'Previous range',
			comparison: 'absolute',
			tab: '0/.0'
		})
		const displayRelativeValues = comparison === 'relative'
		const settings = clientSettings.getModel()
		const kpis = (AnalyticsRobin.getResult('quality_keymetrics') || { data : {}}).data

		const siteLevel: Array<{value: string, text: string}> = []
		if (settings && settings[0] && settings[0].siteMapping)
			settings[0].siteMapping.map(item => siteLevel.push({value: item.name, text: item.alias}))

		const compareBenchmark = this.state.compareBenchmark && !displayRelativeValues
		const qualityDashboard = (
			<DashboardContainer style={{backgroundColor: '#F9FBFF', padding: '1rem', position: 'relative'}}>
				{(clientSettings.isLoading(clientSettings.ACTIONS.FIND_ONE)||clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)) ? <Loader error={clientSettings.isError(clientSettings.ACTIONS.FIND_ONE)}/> : null}
				<div className='dashboard-top-bar'>
					<div style={{display: 'flex', padding: '1rem'}}>
						<DashboardFilter
							width={130}
							type='single'
							title='SITE'
							defaultValue={site}
							style={{marginRight: '1rem'}}
							options={[{value: 'All', text: 'All sites'}, ...siteLevel]}
							onChange={(selected) => this.pageRedirect({site: selected}, true)
						}/>
						<DateRangeFilter value={dateRange} customOption financialStartYear={moment(this.state.filter)} label='DATE RANGE' style={{minWidth: 130, marginRight: '1rem'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
						}/>
						<DashboardFilter
							width={150}
							type='single'
							text='Compared to'
							title='BENCHMARK'
							defaultValue={benchmark}
							options={[{value: 'Previous range', text: 'Previous period'}, {value: 'industry', text: 'Sector'}, {value: 'SegmentSize', text: 'Size'}]}
							onChange={(selected) => this.pageRedirect({benchmark: selected}, true)
							}/>
						<DashboardFilter
							type='switch'
							text='Display Benchmark'
							checked={compareBenchmark}
							onChange={(selected) => this.setState({compareBenchmark: selected})
							}/>
					</div>
				</div>
				<Dashboard
					structure={{
						items : [4, 4],
						size : 4
					}}>
					<DashboardSection section={{
						title : 'Feedback quality metrics',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<KpiChart
								kpis={[
									{
										title: 'NPS',
										loading: AnalyticsRobin.isLoading('quality_keymetrics'),
										error: AnalyticsRobin.isError('quality_keymetrics'),
										data : numberFormater(kpis.nps),
										footer: compareBenchmark ? `${relative(kpis.nps, kpis.npsbenchmark)} Vs Benchmark (${numberFormater(kpis.npsbenchmark)})` : ''
									}, {
										title: 'Sentiment score',
										loading: AnalyticsRobin.isLoading('quality_keymetrics'),
										error: AnalyticsRobin.isError('quality_keymetrics'),
										data : numberFormater(kpis.sentiments*100),
										footer: compareBenchmark ? `${relative(kpis.sentiments, kpis.sentimentsbenchmark)} Vs Benchmark (${numberFormater(kpis.sentimentsbenchmark)})` : ''
									}, {
										title: 'Complaints',
										loading: AnalyticsRobin.isLoading('quality_keymetrics'),
										error: AnalyticsRobin.isError('quality_keymetrics'),
										data : numberFormater(kpis.complaints),
										footer: compareBenchmark ? `${relative(kpis.complaints, kpis.complaintsbenchmark)} Vs Benchmark (${numberFormater(kpis.complaintsbenchmark)})` : ''
									}, {
										title : 'Compliments',
										loading: AnalyticsRobin.isLoading('quality_keymetrics'),
										error: AnalyticsRobin.isError('quality_keymetrics'),
										data : numberFormater(kpis.compliments),
										footer: compareBenchmark ? `${relative(kpis.compliments, kpis.complimentsbenchmark)} Vs Benchmark (${numberFormater(kpis.complimentsbenchmark)})` : ''
									}
								]}
							/>
						</DashboardTab>
					</DashboardSection>
					<DashboardSection
					defaultActiveTab={tab}
					onTabChange={(selected) => this.pageRedirect({tab: selected})}
					section={{
						tabTitles : ['Complaints', 'Compliments'],
						title : 'Feedback word clouds',
						tooltip : 'Tooltip..'
					}}>
						<DashboardTab>
							<WordCloud
								words={(AnalyticsRobin.getResult('quality_wordcloud_complaints') || { data : []}).data}
							/>
						</DashboardTab>
						<DashboardTab>
							<WordCloud
								words={(AnalyticsRobin.getResult('quality_wordcloud_compliments') || { data : []}).data}
							/>
						</DashboardTab>
					</DashboardSection>
				</Dashboard>
			</DashboardContainer>
		)
		return(
			<ErrorBoundary>
				<Tabs selectedDefault={3} fillContainer className= 'tab' style={{fontWeight: 'bold'}}
				active={(current) => {
					switch (current) {
						case 0:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/engagement`)
							break;
						case 1:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/health`)					
							break;
						case 2:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/effectiveness`)					
							break;
						case 4:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/roi`)					
							break;
						case 5:
							this.props.history.push(`/analysis/client-prioritisation/${clientID}/dashboard/cost-to-serve`)					
							break;
					}
				}}
				>
					<TabPane label='Engagement' disabled={!hasPermission('/view/dashboard/engagement', PermissionsRobin.getResult('own-permissions')) }/>
					<TabPane label='Health' disabled={!hasPermission('/view/dashboard/health', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Effectiveness' disabled={!hasPermission('/view/dashboard/effectiveness', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='Quality'>
						{hasPermission('/view/dashboard/quality', PermissionsRobin.getResult('own-permissions')) ? qualityDashboard : noPermission}
					</TabPane>
					<TabPane label='ROI' disabled={!hasPermission('/view/dashboard/roi', PermissionsRobin.getResult('own-permissions'))}/>
					<TabPane label='CostToServe' disabled={!hasPermission('/view/dashboard/cost-to-serve', PermissionsRobin.getResult('own-permissions'))}/>
				</Tabs>
			</ErrorBoundary>
		)
	}

}
export default Quality;