import * as React from 'react'
import {Select, DatePicker, Card} from '@simplus/siui'
import {Divider, Tooltip, Icon} from 'antd'
import {connectRobin} from '@simplus/robin-react'
import {robins} from '../../../robins'
import {RouteComponentProps } from 'react-router-dom';
// import {hasPermission} from '../../../utils'
import {ErrorBoundary} from '../../../utils/ErrorBoundary'
import * as _ from 'lodash'
import * as moment from 'moment'
import * as queryString from 'query-string'
const Option = Select.Option;
const {PermissionsRobin, SummaryRobin, UsersRobin} = robins;

@connectRobin([PermissionsRobin, SummaryRobin, UsersRobin])
export class IcasSummary extends React.Component<RouteComponentProps<{id: string}>> {
	location = ''
	unlisten?: () => void = undefined
	componentWillMount(): void {
		this.applyFilters()
		UsersRobin.find()
		this.unlisten = this.props.history.listen((location, action) => {
			if (action === 'POP')
				this.applyFilters()
		});
	}
	applyFilters(): void {
		if ((!this.location) || (this.location !== this.props.location.search)) {
			const startDate = this.urlState().startDate
			const endDate = this.urlState().endDate
			const client = this.props.match.params.id
			const options: any = {
				startdate : startDate,
				enddate : endDate,
			}
			SummaryRobin.findOne(client, {...options})
		}
	}
	componentWillUnmount(): void {
		if (this.unlisten)
			this.unlisten();
	}
	pageRedirect(update: any, refrsh: boolean = false): void {
		const state = {
			...this.urlState(),
			...update}
		this.props.history.push(`${this.props.location.pathname}?${queryString.stringify(state)}`)
		if (refrsh)
			this.applyFilters()
	}

	urlState(nodefault?: boolean): any {
		if (nodefault)
			return queryString.parse(location.hash.split('?')[1]) || {}
		return _.defaults(queryString.parse(location.hash.split('?')[1]) || {}, this.defaultQuery)
	}

	defaultQuery = {
		dateRange : 'past 30 days',
		risk: '10',
		startDate : moment().subtract(1, 'month').toISOString(),
		endDate : moment().toISOString()
	}

	render(): JSX.Element {
		const risk = this.urlState().risk
		const data = SummaryRobin.getModel() || []
		const filtered_data = (risk === 'all') ? data : data.slice(0, parseInt(risk))
		const usersList = UsersRobin.getCollection() || []
		return(
			<ErrorBoundary>
				<div className='summary'>
					<div className='summary-top-bar'>
						<h1 className='analysis-tab-title'>Summary</h1>
						{/* Might be added in future */}
						{/* {hasPermission('/view/summary/edit-summary', PermissionsRobin.getResult('own-permissions')) ?
						<Button className='new-risk-tag'>New risk tag</Button>
						: null} */}
					</div>
					<Divider/>
					<div className='summary-filters'>
						<DatePicker
							value={this.urlState().dateRange}
							customOption
							label='DATE RANGE'
							style={{marginRight: '1rem', minWidth: '120px'}}
							onChange={(selected) => this.pageRedirect({
								dateRange: selected.value,
								startDate: moment(selected.startDate).toISOString(),
								endDate: moment(selected.endDate).toISOString()
							}, true)
							}>
							<Option value = 'past 30 days'>Past Month</Option>
							<Option value = 'past 90 days'>Past Quarter</Option>
							<Option value = 'past year'>Past Year</Option>
						</DatePicker>
						<Select
							value={this.urlState().risk}
							label='RISK TAG'
							style={{marginRight: '1rem', minWidth: 100}}
							onChange={(selected) => this.pageRedirect({
								risk: selected
							}, true)
							}>
							<Option value = '10'>Top 10 risk tags</Option>
							<Option value = '20'>Top 20 risk tags</Option>
							<Option value = 'all'>All risk tags</Option>
						</Select>
					</div>
					<div className='summary-content'>
					<Card padding style={{width: '70rem'}} loading={SummaryRobin.isLoading(SummaryRobin.ACTIONS.FIND_ONE)}>
					<div className='summary-content-title'>
							<span style={{marginRight: '0.5rem'}}>Risk factors</span>
							<Tooltip title='Tooltip...'>
								<Icon style={{color: '#909090'}} type='info-circle' />
							</Tooltip>
						</div>
						{
							filtered_data.map(item => {
								let title = ''
								let description = ''
								switch (item.type) {
									case 'change_of_custodian':
										title = 'Custodian changed'
										const previous_custodian = usersList.find(user => user._id === item.data.old) || {name: ''}
										const new_custodian = usersList.find(user => user._id === item.data.new) || {name: ''}
										description = `The custodian has been changed from ${previous_custodian.name} to ${new_custodian.name}`
										break;
									case 'low_referals':
										title = 'Low referals'
										description = `Formal referals is at ${item.body.formalreferals} which has fallen below benchmark of ${item.body.formalreferalsbenchmark}`
										break;
									case 'low_engagement':
										title = 'Low engagement'
										description = `Engagement utilisation is at ${(item.body.utilization * 100).toFixed(2)}% which has fallen below benchmark of ${(item.body.utilizationbenchmark * 100).toFixed(2)}%`
										break;
									default:
										description = 'Unknown event type'
								}
								return <div className='summary-item' key={item._id}>
										<div className='summary-item-date'>{moment(item.date, 'X').format('dddd, MMMM Do YYYY, h:mm a')}</div>
											<div className='summary-item-row'>
											<img src='/src/assets/img/summary.png'/>
											<div>
												<div className='summary-event'>{title}</div>
												<div className='summary-description'>{description}</div>
											</div>
										</div>
									</div>
								})
							}
						</Card>
					</div>
				</div>
			</ErrorBoundary>
		)
	}

}
export default IcasSummary;